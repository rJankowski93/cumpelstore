-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 10 Paź 2016, 22:26
-- Wersja serwera: 10.0.17-MariaDB
-- Wersja PHP: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `cumpelstore`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `address`
--

CREATE TABLE `address` (
  `Id` int(11) NOT NULL,
  `Country` varchar(200) DEFAULT NULL,
  `City` varchar(200) DEFAULT NULL,
  `Zip_Code` varchar(200) DEFAULT NULL,
  `Street` varchar(200) DEFAULT NULL,
  `House_Number` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `address`
--

INSERT INTO `address` (`Id`, `Country`, `City`, `Zip_Code`, `Street`, `House_Number`) VALUES
(5, 'Andorra', 'St. Petersburg', '26-398', 'P.O. Box 875, 197 Cum Street', '19'),
(6, 'Algeria', 'Cittanova', '33049', '219-2177 Porttitor Rd.', '70'),
(7, 'Bosnia and Herzegovina', 'Nieuwerkerken', '57-651', 'P.O. Box 937, 6662 Nulla. St.', '78'),
(8, 'Norfolk Island', 'São José', '00-206', '7350 Dignissim. St.', '44'),
(9, 'Australia', 'Poggio Sann Marcello', '9041', '3582 Hendrerit St.', '2'),
(10, 'Ukraine', 'Weston-super-Mare', '5564', 'P.O. Box 306, 3509 Nulla Rd.', '51'),
(11, 'Congo (Brazzaville)', 'Mantova', '4987OR', '617-6324 Diam. St.', '14'),
(12, 'Kyrgyzstan', 'Unnao', '5444DE', 'Ap #930-9611 Donec Street', '67'),
(13, 'Antigua and Barbuda', 'Savona', '8691', 'P.O. Box 734, 1282 Felis Ave', '95'),
(14, 'French Southern Territories', 'Grand Falls', '1884', '4714 Magnis Road', '92'),
(15, 'Bahamas', 'Ichtegem', '94034', '865-1485 Ultrices Av.', '37'),
(16, 'Dominica', 'Orvieto', '760149', 'P.O. Box 271, 5167 Cras Avenue', '2'),
(17, 'Burundi', 'Pirque', '5625TK', 'Ap #760-5342 Ut Avenue', '93'),
(18, 'Lesotho', 'Ajax', '29609', '8386 Porttitor Rd.', '70'),
(19, 'Vanuatu', 'Hamilton', '96669', '956-1757 Ante St.', '42'),
(20, 'Denmark', 'Chesterfield', 'DP78 3KR', 'P.O. Box 884, 4950 Mauris Ave', '26'),
(21, 'Mauritania', 'Cerami', '81991', 'Ap #267-7285 Et, Av.', '34'),
(22, 'Saint Kitts and Nevis', 'Kakisa', '2177', 'P.O. Box 969, 8828 Iaculis Av.', '68'),
(23, 'Guinea-Bissau', 'Arvier', '77480', '544-9614 Nunc Road', '37'),
(24, 'Saint Kitts and Nevis', 'Dubuisson', '11119', 'P.O. Box 151, 541 Donec Road', '22'),
(25, 'Marshall Islands', 'Fratta Todina', '9958', '2428 Turpis Street', '66'),
(41, 'Algeria', 'Cittanova', '33049', '219-2177 Porttitor Rd.', '70'),
(43, 'Bosnia and Herzegovina', 'Nieuwerkerken', '57-651', 'P.O. Box 937, 6662 Nulla. St.', '78'),
(45, 'Ha', 'Tychy', '277', 'Lol', '12'),
(46, 'Poland', 'Tychy', '43-100', 'Paprocanska', '198'),
(67, 'Poland', 'Tychy', '43-100', 'Paprocanska', '198'),
(68, '', '', '', '', ''),
(81, 'Poland', 'Tychy', '43-100', 'Lol', '198'),
(82, '', '', '', '', ''),
(83, 'Poland', 'Tychy', '43-100', 'Paprocanska', '198'),
(84, '', '', '', '', ''),
(85, 'Poland', 'Tychy', '43-100', 'Paprocanska', '198'),
(87, 'Poland', 'Tychy', '43-100', 'Paprocanska', '198'),
(88, 'Poland', 'Tychy', '43-100', 'Paprocanska', '12'),
(89, 'Ha', 'Tychy', '43-100', 'Paprocanska', '198'),
(90, 'janusz', 'janusz', '43-100', 'janusz', '198'),
(91, 'janeczek', 'janeczek', '43-100', 'janeczek', '198'),
(92, 'abcd', 'abcd', '43-100', 'abcd', '198'),
(94, 'Poland', NULL, NULL, NULL, NULL),
(95, 'Poland', 'Tychy', '43=100', 'Paprocanska', '12'),
(96, 'Poland', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cart`
--

CREATE TABLE `cart` (
  `Id` int(255) NOT NULL,
  `Grand_Total` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `cart`
--

INSERT INTO `cart` (`Id`, `Grand_Total`) VALUES
(5, 234),
(6, 234),
(8, 234),
(10, 234),
(11, 234),
(14, 234),
(15, 234),
(16, 234),
(17, 234),
(18, 234),
(19, 234),
(22, 234),
(23, 234),
(24, 1006),
(25, 1600),
(26, 800),
(27, 639),
(28, 1166),
(29, 7200),
(30, 2400),
(31, 1600);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cartitem`
--

CREATE TABLE `cartitem` (
  `Id` int(255) NOT NULL,
  `Product_Id` int(50) DEFAULT NULL,
  `Quantity` int(255) NOT NULL,
  `Total_Price` int(50) NOT NULL,
  `Cart_Id` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `cartitem`
--

INSERT INTO `cartitem` (`Id`, `Product_Id`, `Quantity`, `Total_Price`, `Cart_Id`) VALUES
(1, 74, 1, 234, 5),
(2, 74, 1, 234, 6),
(3, 74, 1, 234, 8),
(4, 74, 1, 234, 10),
(5, 74, 1, 234, 11),
(6, 74, 1, 234, 14),
(7, 74, 1, 234, 15),
(8, 74, 1, 234, 16),
(9, 74, 1, 234, 17),
(10, 74, 1, 234, 18),
(11, 74, 1, 234, 19),
(14, 74, 1, 234, 22),
(15, 74, 1, 234, 23),
(16, 74, 1, 234, 24),
(17, 76, 2, 772, 24),
(18, 74, 2, 1600, 25),
(19, 74, 1, 800, 26),
(23, 75, 1, 639, 27),
(24, 76, 2, 772, 28),
(25, 79, 2, 394, 28),
(26, 74, 9, 7200, 29),
(27, 74, 3, 2400, 30),
(28, 74, 2, 1600, 31);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `customer`
--

CREATE TABLE `customer` (
  `Id` int(255) NOT NULL,
  `Firstname` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Lastname` varchar(50) CHARACTER SET utf8 COLLATE utf8_polish_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Registration_Date` date DEFAULT NULL,
  `User_Id` int(50) UNSIGNED DEFAULT NULL,
  `Address_Id` int(50) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Zrzut danych tabeli `customer`
--

INSERT INTO `customer` (`Id`, `Firstname`, `Lastname`, `email`, `Registration_Date`, `User_Id`, `Address_Id`) VALUES
(31, 'Burton', 'Guthrie', '', '2016-07-25', 46, 8),
(32, 'Aladdin', 'Alvarado', '', '2016-07-25', 9, 9),
(33, 'Holmes', 'Merritt', '', '2016-07-09', 10, 10),
(34, 'Justin', 'Dominguez', '', '2015-11-28', 11, 11),
(35, 'Howard', 'Carlson', '', '2016-04-09', 12, 12),
(36, 'Lionel', 'Odonnell', '', '2016-05-20', 13, 13),
(37, 'Dillon', 'Dotson', '', '2016-03-07', 14, 14),
(38, 'Gavin', 'Ballard', '', '2015-07-29', 15, 15),
(39, 'Carl', 'Montoya', '', '2017-04-29', 16, 16),
(40, 'Beck', 'Valdez', '', '2016-04-15', 17, 17),
(41, 'Edan', 'Shepherd', '', '2016-10-02', 18, 18),
(42, 'Jarrod', 'Alford', '', '2017-07-18', 19, 19),
(43, 'Nicholas', 'Burch', '', '2017-01-01', 20, 20),
(44, 'Jesse', 'Mcdonald', '', '2018-07-18', 21, 21),
(45, 'Leo', 'Rollins', '', '2015-12-13', 22, 22),
(46, 'Eaton', 'Alvarez', '', '2016-02-02', 23, 23),
(47, 'Ishmael', 'Rasmussen', '', '2015-09-15', 24, 24),
(48, 'Rudyard', 'Hobbs', '', '2017-02-19', 25, 25),
(57, 'Paula', 'Sabik', '', '2016-07-25', 47, 45),
(58, 'Rafa?', 'Jankowski', '', '2016-08-03', 48, 46),
(59, 'Olegg', 'Jankowski', '', '2016-08-04', 55, 67),
(63, 'Rafa?', 'Jankowski', '', '2016-08-04', 68, 83),
(65, 'Rafa?', 'Jankowski', '', '2016-08-30', 70, 85),
(66, 'Olegg', 'Jankowski', '', '2016-09-22', 72, 87),
(67, 'Rafa?', 'Jankowski', '', '2016-09-22', 73, 88),
(68, 'Rafa?', 'Jankowski', '', '2016-09-22', 74, 89),
(69, 'janusz', 'janusz', '', '2016-09-22', 75, 90),
(70, 'janeczek', 'janeczek', '', '2016-09-22', 76, 91),
(71, 'abcdd', 'abcd', NULL, NULL, 77, 92),
(72, 'imie', 'nazwisko', 'piotrek.michal@gmail.com', '2016-10-03', 78, 94),
(73, 'imiecfdg', 'nazwiskofdg', NULL, '2016-10-05', 79, 95),
(74, 'janiuu', 'janiuu', 'janiu25@gmail.com', '2016-10-05', 80, 96);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `new_order`
--

CREATE TABLE `new_order` (
  `Id` int(255) NOT NULL,
  `Date` date NOT NULL,
  `State` varchar(255) NOT NULL,
  `Customer_Id` int(255) DEFAULT NULL,
  `Cart_Id` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `new_order`
--

INSERT INTO `new_order` (`Id`, `Date`, `State`, `Customer_Id`, `Cart_Id`) VALUES
(14, '2016-08-30', 'Finished', 32, 5),
(15, '2016-08-30', 'Finished', 32, 6),
(16, '2016-08-31', 'Finished', 32, 8),
(17, '2016-08-31', 'Finished', 32, 10),
(18, '2016-08-31', 'Finished', 32, 11),
(19, '2016-08-31', 'Finished', 32, 14),
(20, '2016-08-31', 'Finished', 32, 15),
(21, '2016-08-31', 'Finished', 32, 16),
(22, '2016-08-31', 'Finished', 32, 17),
(23, '2016-08-31', 'Finished', 32, 18),
(24, '2016-09-02', 'Placed', NULL, 19),
(26, '2016-09-02', 'Placed', NULL, 22),
(27, '2016-09-02', 'Placed', NULL, 23),
(28, '2016-09-02', 'Placed', NULL, 24),
(29, '2016-09-23', 'Placed', NULL, 25),
(30, '2016-09-23', 'Placed', NULL, 26),
(31, '2016-09-24', 'Placed', NULL, 27),
(32, '2016-09-24', 'Placed', NULL, 28),
(33, '2016-10-05', 'Placed', 72, 29),
(34, '2016-10-05', 'Placed', 72, 30),
(35, '2016-10-05', 'Placed', 74, 31);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `product`
--

CREATE TABLE `product` (
  `Id` int(255) NOT NULL,
  `Code` varchar(200) CHARACTER SET utf8 NOT NULL,
  `Name` varchar(500) CHARACTER SET utf8 NOT NULL,
  `Price` float UNSIGNED DEFAULT NULL,
  `Description` varchar(500) COLLATE utf8_bin NOT NULL,
  `Manufacturer` varchar(500) COLLATE utf8_bin NOT NULL,
  `Category` varchar(500) COLLATE utf8_bin NOT NULL,
  `UnitsInStock` int(255) NOT NULL,
  `State` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `is_Promotion` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Zrzut danych tabeli `product`
--

INSERT INTO `product` (`Id`, `Code`, `Name`, `Price`, `Description`, `Manufacturer`, `Category`, `UnitsInStock`, `State`, `is_Promotion`) VALUES
(74, '8297', 'Klor-Con M200', 800, 'dictum eleifend, nunc risus varius orci, in consequat enim diam', 'Lavasoft', 'Laptop', 26, 'New', NULL),
(75, '3116', 'Ciprofloxacin HCl', 639, 'cursus et, eros. Proin ultrices. Duis volutpat nunc sit amet', 'Finale', 'Tablet', 6, 'New', 0),
(76, '960322', 'Klor-Con M200', 2345, '', 'Lenovo', 'Tablet', 5, 'New', NULL),
(77, '7509', 'Ibuprofen (Rx)', 765, 'egestas. Fusce aliquet magna a neque. Nullam ut nisi a', 'Apple Systems', 'Phone', 15, 'Secondhand', 0),
(78, '7901', 'Vyvanse', 681, 'sed dui. Fusce aliquam, enim nec tempus scelerisque, lorem ipsum', 'Macromedia', 'Monitor', 19, 'New', 1),
(79, '4453', 'Zolpidem Tartrate', 197, 'odio. Nam interdum enim non nisi. Aenean eget metus. In', 'Adobe', 'Monitor', 3, 'New', 0),
(80, '2429', 'Furosemide', 513, 'ac risus. Morbi metus. Vivamus euismod urna. Nullam lobortis quam', 'Adobe', 'Monitor', 28, 'Secondhand', 1),
(81, '1493', 'Symbicort', 120, 'Curabitur ut odio vel est tempor bibendum. Donec felis orci,', 'Lavasoft', 'Monitor', 1, 'Secondhand', 0),
(82, '8795', 'Fluoxetine HCl', 536, 'tempus, lorem fringilla ornare placerat, orci lacus vestibulum lorem, sit', 'Lavasoft', 'Phone', 13, 'Secondhand', 0),
(84, '4977', 'Viagra', 593, 'tellus. Aenean egestas hendrerit neque. In ornare sagittis felis. Donec', 'Apple Systems', 'Monitor', 14, 'Secondhand', 0),
(86, '2248', 'Atenolol', 918, 'eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis', 'Borland', 'Phone', 20, 'New', 0),
(87, '5461', 'Amoxicillin Trihydrate/Potassium Clavulanate', 375, 'vitae velit egestas lacinia. Sed congue, elit sed consequat auctor,', 'Lycos', 'Phone', 14, 'Secondhand', 1),
(88, '7208', 'Seroquel', 978, 'massa. Quisque porttitor eros nec tellus. Nunc lectus pede, ultrices', 'Yahoo', 'Monitor', 21, 'Secondhand', 1),
(89, '9666', 'Citalopram HBr', 307, 'amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus', 'Altavista', 'Monitor', 23, 'New', 0),
(90, '8022', 'Paroxetine HCl', 397, 'magna nec quam. Curabitur vel lectus. Cum sociis natoque penatibus', 'Sibelius', 'Phone', 27, 'Secondhand', 0),
(91, '7320', 'Allopurinol', 495, 'elit erat vitae risus. Duis a mi fringilla mi lacinia', 'Borland', 'Monitor', 30, 'New', 0),
(92, '8323', 'Advair Diskus', 650, 'varius orci, in consequat enim diam vel arcu. Curabitur ut', 'Borland', 'Tablet', 20, 'New', 0),
(93, '4438', 'Alprazolam', 564, 'fringilla mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat', 'Cakewalk', 'Monitor', 1, 'Secondhand', 1),
(94, '9997', 'Tramadol HCl', 856, 'Nullam scelerisque neque sed sem egestas blandit. Nam nulla magna,', 'Apple Systems', 'Tablet', 17, 'New', 1),
(95, '5895', 'Paroxetine HCl', 929, 'egestas hendrerit neque. In ornare sagittis felis. Donec tempor, est', 'Yahoo', 'Monitor', 7, 'New', 0),
(96, '5777', 'Premarin', 544, 'feugiat metus sit amet ante. Vivamus non lorem vitae odio', 'Macromedia', 'Laptop', 13, 'New', 0),
(97, '1198', 'Loestrin 24 Fe', 696, 'odio, auctor vitae, aliquet nec, imperdiet nec, leo. Morbi neque', 'Microsoft', 'Laptop', 21, 'Secondhand', 1),
(99, '4196', 'Flovent HFA', 51, 'nec, mollis vitae, posuere at, velit. Cras lorem lorem, luctus', 'Google', 'Monitor', 6, 'Secondhand', 0),
(100, '4126', 'Tricor', 631, 'lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper, velit in', 'Lycos', 'Phone', 9, 'New', 0),
(103, '2757', 'APAP/Codeine', 196, 'orci luctus et ultrices posuere cubilia Curae; Phasellus ornare. Fusce', 'Lavasoft', 'Phone', 28, 'New', 1),
(104, '7278', 'Sertraline HCl', 213, 'ipsum cursus vestibulum. Mauris magna. Duis dignissim tempor arcu. Vestibulum', 'Lycos', 'Phone', 20, 'Secondhand', 0),
(106, '9603', 'Tablet Lenovo Yogaa', 1, '', 'Lenovo', 'Tablet', 5, 'New', NULL),
(107, '12345', 'Osoba', 1, '', 'Lenovo', 'Tablet', 2, 'New', NULL),
(112, '2312313', 'aaaaa', 223, '', 'Lenovo', 'Tablet', 2, 'New', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `Id` int(11) NOT NULL,
  `Login` varchar(200) COLLATE utf8_bin NOT NULL,
  `Password` varchar(300) CHARACTER SET latin1 NOT NULL,
  `Role` varchar(50) COLLATE utf8_bin NOT NULL,
  `Enabled` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`Id`, `Login`, `Password`, `Role`, `Enabled`) VALUES
(1, 'janiu', '$2a$05$.0z5ZHjQLNhYCUkkMon2COk7kXL6GDHkvTZyFN5PfvGL2XLGfQr7C', 'ROLE_ADMIN', 1),
(6, 'libero', 'consequat,', 'ROLE_USER', 1),
(7, 'semper.', 'Cum', 'ROLE_USER', 0),
(8, 'Integer', 'nibh', 'ROLE_USER', 0),
(9, 'elitt.', 'lorem', 'ROLE_USER', 0),
(10, 'varius', 'mi', 'ROLE_USER', 1),
(11, 'dolor.', 'tempus', 'ROLE_USER', 0),
(12, 'dui.', 'morbi', 'ROLE_USER', 0),
(13, 'Suspendisse', 'a', 'ROLE_USER', 0),
(14, 'ullamcorper', 'In', 'ROLE_USER', 1),
(15, 'sem,', 'lobortis', 'ROLE_USER', 0),
(16, 'in', 'a,', 'ROLE_USER', 0),
(17, 'at,', 'facilisis.', 'ROLE_USER', 1),
(18, 'laoreet', 'Curae;', 'ROLE_USER', 0),
(19, 'In', 'Nullam', 'ROLE_USER', 0),
(20, 'nec,', 'Curabitur', 'ROLE_USER', 1),
(21, 'accumsan', 'Nam', 'ROLE_USER', 1),
(22, 'nec', 'lectus', 'ROLE_USER', 1),
(23, 'turpis', 'Nulla', 'ROLE_USER', 0),
(24, 'Nulla', 'dui', 'ROLE_USER', 0),
(25, 'Phasellus', 'vel', 'ROLE_USER', 1),
(41, 'libero', 'consequat,', 'ROLE_USER', 0),
(43, 'semper.', 'Cum', 'ROLE_USER', 0),
(44, 'semper.', 'Cum', 'ROLE_USER', 0),
(45, 'semper.', 'Cum', 'ROLE_USER', 0),
(46, 'Integer', 'nibh', 'ROLE_USER', 0),
(47, 'abc', 'abc', 'ROLE_USER', 0),
(48, 'eu', 'rafi', 'ROLE_USER', 0),
(55, 'abcdd', 'rafi', 'ROLE_USER', 0),
(56, '', '', '', 0),
(66, 'eu', 'asdasd', 'ROLE_USER', 0),
(67, '', '', '', 0),
(68, 'eu', 'rafi', 'ROLE_USER', 0),
(69, 'rafi', '', '', 0),
(70, 'euff', 'rafi', 'ROLE_USER', 0),
(72, 'rafiii', 'rafi', 'ROLE_USER', 0),
(73, 'euuuu', 'rafi', 'ROLE_USER', 0),
(74, 'euuuuuu', 'raf', 'ROLE_USER', 0),
(75, 'janusz', 'janusz', 'ROLE_USER', 0),
(76, 'janeczek', '$2a$05$.0z5ZHjQLNhYCUkkMon2COk7kXL6GDHkvTZyFN5PfvGL2XLGfQr7C', 'ROLE_USER', 0),
(77, 'abcd', '$2a$10$m.dmxhGIZgymlbNQmC57K.bTGev1yo35lP5RfE0hzG7t9B5WKsrBu', 'ROLE_USER', 0),
(78, 'imie', '$2a$10$il1RuG51uyjA8Lzxfp3DWOLul6zBcH7/hvicZWWMqbhLTTU5s/je.', 'ROLE_USER', 1),
(79, 'eugggggg', '$2a$10$S2b4BsFPjBVMQIy6PjUTz.ShIwfaXaoMM6/NQgBjoffclWmgNI016', 'ROLE_USER', 0),
(80, 'janiuu', '$2a$10$.dqr0hjo4qAqbJhzNJfZz.kQv/IndxW8carey8iRKS3YBfFqCDsoK', 'ROLE_USER', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_role`
--

CREATE TABLE `user_role` (
  `Id` int(11) NOT NULL,
  `User_Login` varchar(45) CHARACTER SET latin1 NOT NULL,
  `Role` varchar(45) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Zrzut danych tabeli `user_role`
--

INSERT INTO `user_role` (`Id`, `User_Login`, `Role`) VALUES
(2, 'janiu', 'ROLE_ADMIN'),
(1, 'janiuu', 'ROLE_USER');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `cartitem`
--
ALTER TABLE `cartitem`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `id` (`Id`),
  ADD KEY `id_2` (`Id`);

--
-- Indexes for table `new_order`
--
ALTER TABLE `new_order`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Code` (`Code`),
  ADD KEY `id` (`Id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `uni_username_role` (`Role`,`User_Login`),
  ADD KEY `fk_username_idx` (`User_Login`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `address`
--
ALTER TABLE `address`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT dla tabeli `cart`
--
ALTER TABLE `cart`
  MODIFY `Id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT dla tabeli `cartitem`
--
ALTER TABLE `cartitem`
  MODIFY `Id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT dla tabeli `customer`
--
ALTER TABLE `customer`
  MODIFY `Id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT dla tabeli `new_order`
--
ALTER TABLE `new_order`
  MODIFY `Id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT dla tabeli `product`
--
ALTER TABLE `product`
  MODIFY `Id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT dla tabeli `user_role`
--
ALTER TABLE `user_role`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
