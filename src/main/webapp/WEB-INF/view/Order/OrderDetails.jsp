<%--
  Created by IntelliJ IDEA.
  User: Rafi
  Date: 24/07/2016
  Time: 12:15
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <title><spring:message code="labels.order.title"/></title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1>
                <spring:message code="labels.order.title"/>
            </h1>
        </div>
    </div>
    <div class="pull-right" style="padding-right: 50px">
        <a href="?language=pl"> <spring:message code="labels.language.pl"/></a> | <a href="?language=en">
        <spring:message code="labels.language.en"/></a>
    </div>
</section>

<section class="container">
    <div class="row">
        <div class="col-md-5">
            <h2><fmt:formatDate value="${order.date}" pattern="dd/MM/yyyy"/></h2>
            <p>
                <strong><spring:message code="labels.order.state"/></strong>: ${order.state}
            </p>
            <h3>
                <strong class="label label-warning"><spring:message code="labels.customer.title"/> </strong>
            </h3>
            <p>
                <strong><spring:message code="labels.customer.firstname"/></strong>: ${order.customer.firstname}
            </p>
            <p>
                <strong><spring:message code="labels.customer.lastname"/></strong>: ${order.customer.lastname}
            </p>
            <p>
                <strong><spring:message code="labels.login.login"/></strong>: ${order.customer.user.login}
            </p>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Code</th>
                        <th>pricePerUnit</th>
                        <th>unitsInOrder</th>
                        <th>price</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${order.products}" var="product" varStatus="loop">
                        <tr>
                            <td>${loop.index+1}</td>
                            <td> ${product.code}</td>
                            <td> ${product.pricePerUnit}</td>
                            <td> ${product.unitsInOrder}</td>
                            <td> ${product.price}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <p>
                <a class="btn btn-default" href="<spring:url value="/orders" />"> <span
                        class="glyphicon glyphicon-hand-left"></span> <spring:message code="labels.return"/>
                </a>
            </p>
        </div>
    </div>
</section>


</body>
</html>
