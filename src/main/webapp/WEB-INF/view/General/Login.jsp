<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
<title><spring:message code="labels.cumpelstore.title" /></title>
</head>
<body>
	<section>
		<div class="jumbotron">
			<div class="container">
				<h1>
					<spring:message code="labels.cumpelstore.title" />
				</h1>
			</div>
		</div>
		<div class="pull-right" style="padding-right: 50px">
			<a href="?language=pl"> <spring:message code="labels.language.pl" /></a> | <a href="?language=en"> <spring:message code="labels.language.en" /></a>
		</div>
	</section>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							<spring:message code="labels.login.login" />
						</h3>
					</div>
					<div class="panel-body">
						<c:if test="${not empty error}">
							<div class="alert alert-danger">
								<spring:message code="labels.error.passowrd" />
								<br />
							</div>
						</c:if>
						<form action="<c:url value="/login"></c:url>" method="post">
							<fieldset>
								<div class="form-group">
									<input class="form-control" placeholder=<spring:message code="labels.login.login"/> name='username' type="text">
								</div>
								<div class="form-group">
									<input class="form-control" placeholder=<spring:message code="labels.login.password"/> name='password' type="password" value="">
									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
								</div>
								<input class="btn btn-lg btn-success btn-block" type="submit" value="Zaloguj">
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>