<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page session="false" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
    <title><spring:message code="labels.registration"></spring:message></title>
</head>
<body>
<h1>
    <spring:message code="labels.registration"></spring:message>
</h1>
<form:form modelAttribute="customer" method="POST" enctype="utf8">
    <form:errors path="*" cssClass="alert alert-danger" element="div"/>
    <br>
    <tr>
        <td>
            <label>
                <spring:message code="labels.customer.firstname"></spring:message>
            </label>
        </td>
        <td><form:input path="firstname" value=""/></td>
    </tr>
    <tr>
        <td>
            <label>
                <spring:message code="labels.customer.lastname"></spring:message>
            </label>
        </td>
        <td><form:input path="lastname" value=""/></td>
    </tr>
    <tr>
        <td>
            <label>
                <spring:message code="labels.customer.email"></spring:message>
            </label>
        </td>
        <td><form:input path="email" value=""/></td>
    </tr>

    <tr>
        <td>
            <label>
                <spring:message code="labels.login.login"></spring:message>
            </label>
        </td>
        <td>
            <form:input path="user.login" value="" type="text"/></td>
        <form:errors path="user" cssClass="text-danger"></form:errors>
    </tr>
    <tr>

    <tr>
        <td>
            <label>
                <spring:message code="labels.login.password"></spring:message>
            </label>
        </td>
        <td>
            <form:input path="user.password" value="" type="password"/></td>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </tr>
    <tr>
        <td>
            <label>
                <spring:message code="labels.login.confirmPassword"></spring:message>
            </label>
        </td>
            <%--<td><form:input path="matchingPassword" value="" type="password" /></td>--%>
        <td><input value="" type="password"></td>
    </tr>

    <tr>
        <td>
            <label>
                <spring:message code="labels.customer.address.country"></spring:message>
            </label>
        </td>
        <td>
            <form:input path="address.country" value="" type="text"/></td>
    </tr>
    <button type="submit">
        <spring:message code="labels.login.submit"></spring:message>
    </button>
</form:form>
<br>
<a href="<c:url value="/login" />">
    <spring:message code="labels.login.login"></spring:message>
</a>
</body>
</html>