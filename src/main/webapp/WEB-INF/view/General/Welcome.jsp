<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet"></link>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.1/angular.min.js"></script>
    <title><spring:message code="labels.welcome.title"/></title>
</head>
<body>
<section class="">
    <div class="jumbotron">
        <div class="container">
            <h1>
                <spring:message code="labels.welcome.title"/>
            </h1>
            <p>
                <spring:message code="labels.cumpelstore.title"/>
            </p>
            <a href="<c:url value="/products" />" class="btn btn-danger btn-mini "><spring:message
                    code="labels.product.list"/></a>
            <a href="<c:url value="/customers" />" class="btn btn-danger btn-mini "><spring:message
                    code="labels.customer.list"/></a>
            <a href="<c:url value="/orders" />" class="btn btn-danger btn-mini "><spring:message
                    code="labels.order.list"/></a>
        </div>
    </div>
    <div class="pull-right" style="padding-right: 50px">
        <a href="?language=pl"> <spring:message code="labels.language.pl"/></a> | <a href="?language=en">
        <spring:message code="labels.language.en"/></a>
    </div>
</section>
</body>
</html>
