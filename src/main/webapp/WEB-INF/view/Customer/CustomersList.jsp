<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglibs/customTaglib.tld" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery.min.js" />"></script>
    <title><spring:message code="labels.customers.title"/></title>

    <script type="text/javascript">
        $(function () {
            $("#registrationDateFrom").datepicker();
            $("#registrationDateTo").datepicker();
        });

        function findCustomersList() {
            var url = "/customers/filter/";
            if ($("#login").val() != "") {
                url = url + "login=" + $("#login").val() + ";"
            }
            if ($("#firstname").val() != "") {
                url = url + "firstname=" + $("#firstname").val() + ";"
            }
            if ($("#lastname").val() != "") {
                url = url + "lastname=" + $("#lastname").val() + ";"
            }
            if ($("#registrationDateFrom").val() != "") {
                url = url + "registrationDateFrom=" + $("#registrationDateFrom").val().replace(/\//g, '-') + ";"
            }
            if ($("#registrationDateTo").val() != "") {
                url = url + "registrationDateTo=" + $("#registrationDateTo").val().replace(/\//g, '-') + ";"
            }
            if ($("#country").val() != "") {
                url = url + "country=" + $("#country").val() + ";"
            }
            if ($("#city").val() != "") {
                url = url + "city=" + $("#city").val() + ";"
            }
            console.log(url);
            if (url == "/customers/filter/") {
                url = "/customers";
            }
            window.location.href = url;
        }

        function getParametrUrlVariable(variable) {
            var pattern = new RegExp(variable + "=([^&#=;]*)");
            var parametr = pattern.exec(window.location.pathname);
            if (parametr != null) {
                return parametr[1];
            }
            return "";
        }

        function setParametrSeachCriteria() {
            $("#login").val(getParametrUrlVariable('login'));
            $("#firstname").val(getParametrUrlVariable('firstname'));
            $("#lastname").val(getParametrUrlVariable('lastname'));
            $("#registrationDateFrom").val(getParametrUrlVariable('registrationDateFrom'));
            $("#registrationDateTo").val(getParametrUrlVariable('registrationDateTo'));
            $("#country").val(getParametrUrlVariable('country'));
            $("#city").val(getParametrUrlVariable('city'));
        }

        function insertParamToUrl(selectedObject) {
            var key = encodeURI(selectedObject.name);
            var value = encodeURI(selectedObject.value);
            var kvp = document.location.search.substr(1).split('&');
            var i = kvp.length;
            var x;
            while (i--) {
                x = kvp[i].split('=');
                if (x[0] == key) {
                    x[1] = value;
                    kvp[i] = x.join('=');
                    break;
                }
            }
            if (i < 0) {
                kvp[kvp.length] = [key, value].join('=');
            }
            document.location.search = kvp.join('&');
        }

        window.addEventListener("load", function () {
            var valueSortBy = String('${orderBy}');
            if (valueSortBy && 0 != valueSortBy.length) {
                $('[name="orderBy"]').val(valueSortBy);
            }
        }, false);

    </script>
</head>
<body onload="setParametrSeachCriteria()">
<section>
    <div class="jumbotron">
        <div class="container">
            <h1>
                <spring:message code="labels.customers.title"/>
            </h1>
            <p>
                <spring:message code="labels.customer.list"/>
            </p>
            <a href="<c:url value="/customers/create" />" class="btn btn-danger btn-mini"><spring:message
                    code="labels.customer.create"/></a><br/>
            <a href="<c:url value="/" />" class="btn btn-danger btn-mini"><spring:message
                    code="labels.return"/></a><br/>
            <div class="pull-right" style="padding-right: 50px">
                <a href="?language=pl"> <spring:message code="labels.language.pl"/></a> | <a href="?language=en">
                <spring:message code="labels.language.en"/></a>
            </div>
        </div>
    </div>
</section>

<section class="container">
    <div class="container">
        <span class="col-md-2">Login</span>
        <input class="col-md-2" type="text" id="login">
        <span class="col-md-2">Firstname</span>
        <input class="col-md-2" type="text" id="firstname">
        <span class="col-md-2">Lastname</span>
        <input class="col-md-2" type="text" id="lastname">
        <span class="col-md-2">Registration Date From</span>
        <input class="col-md-2" type="text" id="registrationDateFrom">
        <span class="col-md-2">Registration Date To</span>
        <input class="col-md-2" type="text" id="registrationDateTo">
        <span class="col-md-2">Country</span>
        <input class="col-md-2" type="text" id="country">
        <span class="col-md-2">City</span>
        <input class="col-md-2" type="text" id="city">
        &nbsp;
        <button class="col-md-2 col-md-offset-5" onclick="findCustomersList()">Find</button>
        &nbsp;
    </div>
    <div class="row">
        <select name="orderBy" onchange="insertParamToUrl(this)">
            <option value="firstnameASC"><spring:message code="labels.customer.sorting.nameASC"/></option>
            <option value="firstnameDESC"><spring:message code="labels.customer.sorting.nameDESC"/></option>
            <option value="registrationDateASC"><spring:message
                    code="labels.customer.sorting.registrationDateASC"/></option>
            <option value="registrationDateDESC"><spring:message
                    code="labels.customer.sorting.registrationDateDESC"/></option>
        </select>
        <c:forEach items="${customers}" var="customer">
            <div class="thumbnail">
                <div class="caption">
                    <h3>${customer.firstname}</h3>
                    <p>${customer.lastname}</p>
                    <p>${customer.address.country}</p>
                    <p>${customer.address.city}</p>
                    <p>${customer.address.street} ${customer.address.houseNumber}</p>
                    <p>
                        <a href=" <spring:url value="/customers/customer?id=${customer.id}" /> "
                           class="btn btn-primary"> <span class="glyphicon-info-sign glyphicon"/></span> <spring:message
                                code="labels.details"/></a>
                        <a href=" <spring:url value="/customers/removeCustomer?id=${customer.id}" /> "
                           class="btn btn-danger"> <span class="glyphicon-info-sign glyphicon"/></span> <spring:message
                                code="labels.remove"/></a>
                        <a href=" <spring:url value="/customers/modifyCustomer?id=${customer.id}" /> "
                           class="btn btn-danger"> <span class="glyphicon glyphicon-pencil glyphicon"/></span>
                            <spring:message code="labels.modify"/></a>
                    </p>
                </div>
            </div>
        </c:forEach>
        <tag:paginate max="15" offset="${offset}" count="${count}"
                      uri="/customers" next="&raquo;" previous="&laquo;"></tag:paginate>
    </div>
</section>

</body>
</html>
