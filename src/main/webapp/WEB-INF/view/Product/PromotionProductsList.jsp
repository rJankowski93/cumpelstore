<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery.min.js" />"></script>
    <title><spring:message code="labels.promotion.products.title"/></title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1>
                <spring:message code="labels.products.title"/>
            </h1>
            <p>
                <spring:message code="labels.product.list"/>
            </p>
        </div>
    </div>
    <div class="pull-right" style="padding-right: 50px">
        <a href="?language=pl"> <spring:message code="labels.language.pl"/></a> | <a href="?language=en">
        <spring:message code="labels.language.en"/></a>
    </div>
</section>
<section class="container">
    <div class="row">
        <c:forEach items="${products}" var="product">
            <!-- <div class="col-sm-6 col-md-3" style="padding-boottom: 15px"> -->
            <div class="thumbnail">
                <img alt="image" src="<c:url value="/resources/images/${product.code}.png"></c:url>" width="22%"
                     height="" align="left">

                <div class="caption">
                    <h3>${product.name}</h3>
                    <p>${product.description}</p>
                    <p>${product.price}PLN</p>
                    <p>
                        <spring:message code="labels.product.count.stock"/>
                            ${product.unitsInStock}
                    </p>
                    <p>
                        <a href=" <spring:url value="/products/product?id=${product.id}" /> " class="btn btn-primary">
                            <span class="glyphicon-info-sign glyphicon"/></span> <spring:message code="labels.details"/>

                        </a>
                    </p>
                </div>
            </div>
            <!-- </div> -->
        </c:forEach>
    </div>
</section>
</body>
</html>
