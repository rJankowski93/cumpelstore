<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery.min.js" />"></script>
    <title><spring:message code="labels.product.create"/></title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1>
                <spring:message code="labels.product.create"/>
            </h1>
        </div>
    </div>
    <div class="pull-right" style="padding-right: 50px">
        <a href="?language=pl"> <spring:message code="labels.language.pl"/></a> | <a href="?language=en">
        <spring:message code="labels.language.en"/></a>
    </div>
</section>
<section class="container">
    <form:form modelAttribute="product" class="form-horizontal" enctype="multipart/form-data">
        <form:errors path="*" cssClass="alert alert-danger" element="div"/>
        <fieldset>
            <legend>
                <spring:message code="labels.product.create"/>
            </legend>

            <div class="form-group">
                <label class="control-label col-lg-2 col-lg-2" for="id"><spring:message code="labels.code"/></label>
                <div class="col-lg-10">
                    <form:input id="code" path="code" type="text" class="form:input-large"/>
                    <form:errors path="code" cssClass="text-danger"></form:errors>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-lg-2" for="name"> <spring:message code="labels.name"/></label>
                <div class="col-lg-10">
                    <form:input id="name" path="name" type="text" class="form:input-large"/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-lg-2" for="price"> <spring:message code="labels.price"/>
                </label>
                <div class="col-lg-10">
                    <div class="form:input-prepend">
                        <form:input id="price" path="price" type="text" class="form:input-large"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-lg-2" for="description"> <spring:message code="labels.description"/>
                </label>
                <div class="col-lg-10">
                    <form:textarea id="description" path="description" rows="2"/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-lg-2" for="manufacturer"> <spring:message code="labels.manufacturer"/>
                </label>
                <div class="col-lg-10">
                    <form:input id="manufacturer" path="manufacturer" type="text" class="form:input-large"/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-lg-2" for="category"> <spring:message code="labels.category"/>
                </label>
                <div class="col-lg-10">
                    <form:input id="category" path="category" type="text" class="form:input-large"/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-lg-2" for="unitsInStock"> <spring:message
                        code="labels.product.count.stock"/>
                </label>
                <div class="col-lg-10">
                    <form:input id="unitsInStock" path="unitsInStock" type="text" class="form:input-large"/>
                </div>
            </div>

                <%-- <div class="form-group">
                    <label class="control-label col-lg-2" for="unitsInOrder"> <spring:message code="labels.product.count.order" />
                    </label>
                    <div class="col-lg-10">
                        <form:input id="unitsInOrder" path="unitsInOrder" type="text" class="form:input-large" />
                    </div>
                </div> --%>

                <%-- <div class="form-group">
                    <label class="control-label col-lg-2" for="discontinued"> <spring:message code="labels.product.discontinued" />
                    </label>
                    <div class="col-lg-10">
                        <form:input id="discontinued" path="discontinued" type="text" class="form:input-large" />
                    </div>
                </div> --%>

            <div class="form-group">
                <label class="control-label col-lg-2" for="state"> <spring:message code="labels.state"/>
                </label>
                <div class="col-lg-10">
                    <form:radiobutton path="state" value="New"/>
                    <spring:message code="labels.new"/>
                    <form:radiobutton path="state" value="Secondhand"/>
                    <spring:message code="labels.secondhand"/>
                    <form:radiobutton path="state" value="Refurbished"/>
                    <spring:message code="labels.refurbished"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2" for="productImage"> <spring:message code="labels.product.image"/>
                </label>
                <div class="col-lg-10">
                    <form:input id="productImage" path="productImage" type="file" class="form:input-large"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" id="btnAdd" class="btn btn-primary" value="Dodaj"/>
                    <a class="btn btn-default" href="<spring:url value="/products" />"> <span
                            class="glyphicon glyphicon-hand-left"></span>
                            <spring:message code="labels.return"/>
                </div>
            </div>

        </fieldset>
    </form:form>
</section>
</body>
</html>
