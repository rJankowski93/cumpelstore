<%--
  Created by IntelliJ IDEA.
  User: Rafi
  Date: 13/07/2016
  Time: 14:28
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery.min.js" />"></script>
    <title><spring:message code="labels.products.title"/></title>
</head>
<body>

<section>
    <div class="jumbotron">
        <div class="container">
            <h1>
                <spring:message code="labels.products.title"/>
            </h1>
            <p>
                <spring:message code="labels.product.list"/>
            </p>
            <a href="<c:url value="/products/create" />" class="btn btn-danger btn-mini"><spring:message
                    code="labels.product.create"/></a><br/>
            <a href="<c:url value="/" />" class="btn btn-danger btn-mini"><spring:message
                    code="labels.return"/></a><br/>
            <input type="text" id="promotionCode">
            <input type="submit" value=
            <spring:message code="labels.promotion.check"/> onclick="checkPromotionCode()">
            <div class="pull-right" style="padding-right: 50px">
                <a href="?language=pl"> <spring:message code="labels.language.pl"/></a> | <a href="?language=en">
                <spring:message code="labels.language.en"/></a>
            </div>
        </div>
    </div>
</section>
</body>
</html>
