<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<sec:authentication var="user" property="principal"/>

<li>
    <h3>
        <sec:authorize access="isAuthenticated()">
            Hello  ${user.username}
        </sec:authorize>
    </h3>
</li>
<li><a href="<spring:url value="/#"/>"><spring:message code="labels.cumpelstore.title"/></a></li>
<li><a href="<spring:url value="/products/"/>"><spring:message code="labels.product.list"/></a></li>
<li><a href="<spring:url value="/products/add"/>"><spring:message code="labels.product.create"/></a></li>
<li><a href="<spring:url value="/cart/"/>"><spring:message code="labels.cart.title"/></a></li>
<li><a href="<spring:url value="/login"/>"><spring:message code="labels.login"/></a></li>
<li><a href="<spring:url value="/logout"/>"><spring:message code="labels.logout"/></a></li>
<li><a href="<spring:url value="/registration"/>"><spring:message code="labels.registration"/></a></li>
