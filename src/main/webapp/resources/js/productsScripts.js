function checkPromotionCode() {
    var code = document.getElementById("promotionCode").value;
    var actionURL = "products/specialOffer?promo=" + code;
    window.location.href = actionURL;
    //document.write(actionURL);
}

function findProductsList() {
    var url = "/products/filter/";
    if ($("#name").val() != "") {
        url = url + "name=" + $("#name").val() + ";"
    }
    if ($("#category").val() != "") {
        url = url + "category=" + $("#category").val() + ";"
    }
    if ($("#manufacturer").val() != "") {
        url = url + "manufacturer=" + $("#manufacturer").val() + ";"
    }
    if ($("#state").val() != "") {
        url = url + "state=" + $("#state").val() + ";"
    }
    if ($("#priceFrom").val() != "") {
        url = url + "priceFrom=" + $("#priceFrom").val() + ";"
    }
    if ($("#priceTo").val() != "") {
        url = url + "priceTo=" + $("#priceTo").val() + ";"
    }
    if (url == "/products/filter/") {
        url = "/products";
    }
    window.location.href = url;
}

function getParametrUrlVariable(variable) {
    var pattern = new RegExp(variable + "=([^&#=;]*)");
    var parametr = pattern.exec(window.location.pathname);
    if (parametr != null) {
        return parametr[1];
    }
    return "";
}

function setParametrSeachCriteria() {
    $("#state").val(getParametrUrlVariable('state'));
    $("#name").val(getParametrUrlVariable('name'));
    $("#category").val(getParametrUrlVariable('category'));
    $("#manufacturer").val(getParametrUrlVariable('manufacturer'));
    $("#priceFrom").val(getParametrUrlVariable('priceFrom'));
    $("#priceTo").val(getParametrUrlVariable('priceTo'));
}

function insertParamToUrl(selectedObject) {
    var key = encodeURI(selectedObject.name);
    var value = encodeURI(selectedObject.value);
    var kvp = document.location.search.substr(1).split('&');
    var i = kvp.length;
    var x;
    while (i--) {
        x = kvp[i].split('=');
        if (x[0] == key) {
            x[1] = value;
            kvp[i] = x.join('=');
            break;
        }
    }
    if (i < 0) {
        kvp[kvp.length] = [key, value].join('=');
    }
    document.location.search = kvp.join('&');
}
