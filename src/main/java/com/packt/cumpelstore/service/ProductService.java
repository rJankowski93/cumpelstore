package com.packt.cumpelstore.service;

import com.packt.cumpelstore.dao.ProductDAO;
import com.packt.cumpelstore.data.ProductData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ProductService {

    @Autowired
    ProductDAO productDAO;

    public List<ProductData> getList(Integer offset, Integer maxResults, String sortBy) {
        return productDAO.getList(offset, maxResults, sortBy);
    }

    public ProductData getItemById(Long id) {
        return productDAO.findOne(id);
    }

    public void removeItem(Long id) {
        productDAO.delete(id);
    }

    public void createItem(ProductData productData) {
        productDAO.save(productData);
    }

    public void saveItem(ProductData productData) {
        productDAO.save(productData);
    }

    public List<ProductData> getItemsByCategory(String category) {
        return productDAO.getItemsByCategory(category);
    }

    public List<ProductData> getProductsByFilter(Map<String, List<Object>> filterParams) {
        return productDAO.getProductsByFilter(filterParams);
    }

    public List<ProductData> getPromotionProductsList() {
        return productDAO.getPromotionProductsList();
    }

    public List<ProductData> getProductByCode(String code) {
        return productDAO.getProductByCode(code);
    }

    public Long countQuantity() {
        return productDAO.countQuantity();
    }
}
