package com.packt.cumpelstore.service;

import com.packt.cumpelstore.dao.OrderDAO;
import com.packt.cumpelstore.data.OrderData;
import com.packt.cumpelstore.data.PayPalData;
import com.packt.cumpelstore.data.ProductData;
import com.packt.cumpelstore.exception.ApplicationException;
import com.packt.cumpelstore.exception.DAOError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {

    @Autowired
    private ProductService productService;

    @Autowired
    private OrderDAO orderDAO;

    public List<OrderData> getList(Integer offset, Integer maxResults, String orderBy) {
        return orderDAO.getList(offset, maxResults, orderBy);
    }

    public OrderData getItemById(Long id) {
        return orderDAO.findOne(id);
    }

    public void removeItem(Long id) {
        orderDAO.delete(id);
    }

    public void createItem(OrderData orderData) {
        orderDAO.createItem(orderData);
    }

    public void saveItem(OrderData orderData) {
        orderDAO.save(orderData);
    }

    public List<OrderData> getItemsByState(String state) {
        return orderDAO.getItemsByState(state);
    }

    public void processOrder(Long id, int count) {
        ProductData productById = productService.getItemById(id);
        if (productById.getUnitsInStock() < count) {
            throw new ApplicationException(DAOError.FEW_PRODUCTS);
        }
        productById.setUnitsInStock(productById.getUnitsInStock() - count);
    }

    public Long countQuantity() {
        return orderDAO.countQuantity();
    }

    public PayPalData getPayPalConfig() {
        return orderDAO.getPayPalConfig();
    }


}
