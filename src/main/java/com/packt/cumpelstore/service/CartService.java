package com.packt.cumpelstore.service;

import com.packt.cumpelstore.dao.CartDAO;
import com.packt.cumpelstore.data.CartData;
import com.packt.cumpelstore.exception.ApplicationException;
import com.packt.cumpelstore.exception.DAOError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Rafi pedal on 07/08/2016.
 */

@Service
public class CartService {

    @Autowired
    CartDAO cartDAO;

    public CartData create(CartData cartData) {
        return cartDAO.create(cartData);
    }

    public CartData read(String cartId) {
        return cartDAO.read(cartId);
    }

    public void update(String cartId, CartData cartData) {
        cartDAO.update(cartId, cartData);
    }

    public void delete(String cartId) {
        cartDAO.delete(cartId);
    }

    public CartData validate(String cartId) {
        CartData cart = cartDAO.read(cartId);
        if (cart == null || cart.getCartItems().isEmpty()) {
            throw new ApplicationException(DAOError.CART_EMPTY);
        }
        return cart;
    }
}
