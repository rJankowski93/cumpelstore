package com.packt.cumpelstore.service;

import com.packt.cumpelstore.dao.UserDAO;
import com.packt.cumpelstore.data.UserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserDAO userDAO;

    public List<UserData> getUserByLogin(String login) {
        return userDAO.getUserByLogin(login);
    }

}
