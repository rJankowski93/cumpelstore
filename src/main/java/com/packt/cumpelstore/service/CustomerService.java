package com.packt.cumpelstore.service;

import com.packt.cumpelstore.dao.CustomerDAO;
import com.packt.cumpelstore.data.CustomerData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

@Service
public class CustomerService {

    @Autowired
    CustomerDAO customerDAO;

    public List<CustomerData> getList(Integer offset, Integer maxResults, String orderBy) {
        return customerDAO.getList(offset, maxResults, orderBy);
    }

    public CustomerData getItemById(Long id) {
        return customerDAO.findOne(id);
    }

    public CustomerData getItemByCurrentUser() {
        return customerDAO.getItemByCurrentUser();
    }

    public void removeItem(Long id) {
        customerDAO.delete(id);
    }

    public void createItem(CustomerData customerData) {
        customerDAO.createItem(customerData);
    }

    public void saveItem(CustomerData customerData) {
        customerDAO.save(customerData);
    }

    public List<CustomerData> getCustomerByFilter(Map<String, List<Object>> filterParams) throws ParseException {
        return customerDAO.getCustomerByFilter(filterParams);
    }

    public Long countQuantity() {
        return customerDAO.countQuantity();
    }
}
