package com.packt.cumpelstore.controller;

import com.packt.cumpelstore.data.ProductData;
import com.packt.cumpelstore.exception.ApplicationException;
import com.packt.cumpelstore.service.ProductService;
import com.packt.cumpelstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/products")
public class ProductController {

    @Autowired
    ProductService productService;

    @Autowired
    UserService userService;

    @RequestMapping()
    public String list(Model model, Integer offset, Integer maxResults, String orderBy) {
        model.addAttribute("products", productService.getList(offset, maxResults, orderBy));
        model.addAttribute("count", productService.countQuantity());
        model.addAttribute("offset", offset);
        model.addAttribute("orderBy", orderBy);
        return "Product/ProductsList";
    }

    @RequestMapping("/product")
    public String getItemById(Model model, @RequestParam("id") Long id) {
        model.addAttribute("product", productService.getItemById(id));
        return "Product/ProductDetails";
    }

    @RequestMapping("/removeProduct")
    public String removeProduct(@RequestParam("id") Long id) {
        productService.removeItem(id);
        return "redirect:/products";
    }

    @RequestMapping(value = "/modifyProduct", method = RequestMethod.GET)
    public String modifyProduct(Model model, @RequestParam("id") Long id) {
        model.addAttribute("product", productService.getItemById(id));
        return "Product/ProductModify";
    }

    @RequestMapping(value = "/modifyProduct", method = RequestMethod.POST)
    public String saveModifiedProduct(@ModelAttribute("product") ProductData product, HttpServletRequest request) {
        MultipartFile image = product.getProductImage();
        String rootDirectory = request.getSession().getServletContext().getRealPath("/");
        if (image != null && !image.isEmpty()) {
            try {
                image.transferTo(new File(rootDirectory + "resources\\images\\" + product.getCode() + ".png"));
            } catch (Exception e) {
                throw new RuntimeException("Error in save image", e);
            }
        }
        productService.saveItem(product);
        return "redirect:/products";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createProduct(Model model) {
        ProductData product = new ProductData();
        model.addAttribute("product", product);
        return "Product/ProductCreate";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String saveNewProduct(@ModelAttribute("product") @Valid ProductData product, BindingResult result, HttpServletRequest request) {
        if (result.hasErrors()) {
            return "Product/ProductCreate";
        }
//        String[] suppressedFields = result.getSuppressedFields();
//        if (suppressedFields.length > 0) {
//            throw new RuntimeException("Proba wiazania niedozwolonych pol: " + StringUtils.arrayToCommaDelimitedString(suppressedFields));
//        }
        MultipartFile image = product.getProductImage();
        String rootDirectory = request.getSession().getServletContext().getRealPath("/");
        if (image != null && !image.isEmpty()) {
            try {
                image.transferTo(new File(rootDirectory + "resources\\images\\" + product.getCode() + ".png"));
            } catch (Exception e) {
                throw new RuntimeException("Error in save image", e);
            }
        }
        productService.createItem(product);
        return "redirect:/products";
    }

    @RequestMapping("/filter/{ByCriteria}")
    public String getProductByFilter(@MatrixVariable(pathVar = "ByCriteria") Map<String, List<Object>> filterParams, Model model) {
        model.addAttribute("products", productService.getProductsByFilter(filterParams));
        return "Product/ProductsList";
    }

    @RequestMapping("/invalidPromoCode")
    public String invalidPromoCode() {
        return "Product/InvalidPromoCode";
    }

    @RequestMapping("/promotionList")
    public String getPromotionList(Model model) {
        model.addAttribute("products", productService.getPromotionProductsList());
        return "Product/PromotionProductsList";
    }

    @RequestMapping(value = "/paypal", method = RequestMethod.GET)
    public String payPal(Model model) {
        return "Product/PayPal";
    }


    //TODO doczytac o co z tym chodzi
    @InitBinder
    public void initialiseBinder(WebDataBinder binder) {
        binder.setDisallowedFields("unitsInOrder", "discontinued");
    }

    @ExceptionHandler(ApplicationException.class)
    public ModelAndView handleError(HttpServletRequest request, ApplicationException exception) {
        ModelAndView mav = new ModelAndView();
        mav.addObject("invalidProduct", exception.getCodeMessage());
        mav.addObject("exception", exception);
        mav.addObject("url", request.getRequestURI() + "?" + request.getQueryString());
        mav.setViewName("Product/ProductNotFound");
        return mav;
    }
}
