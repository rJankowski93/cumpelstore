package com.packt.cumpelstore.controller;

import com.packt.cumpelstore.data.CartData;
import com.packt.cumpelstore.data.CartItemData;
import com.packt.cumpelstore.data.ProductData;
import com.packt.cumpelstore.exception.ApplicationException;
import com.packt.cumpelstore.exception.DAOError;
import com.packt.cumpelstore.service.CartService;
import com.packt.cumpelstore.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value = "rest/cart")
public class CartRestController {

    public CartService cartService;

    public ProductService productService;

    @Autowired
    public CartRestController(ProductService productService, CartService cartService) {
        this.productService = productService;
        this.cartService = cartService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    CartData create(@RequestBody CartData cart) {
        return cartService.create(cart);
    }

    @RequestMapping(value = "/{cartId}", method = RequestMethod.GET)
    public
    @ResponseBody
    CartData read(@PathVariable(value = "cartId") String cartId) {
        return cartService.read(cartId);
    }

    @RequestMapping(value = "/{cartId}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void update(@PathVariable(value = "cartId") String cartId, @RequestBody CartData cart) {
        cartService.update(cartId, cart);
    }

    @RequestMapping(value = "/{cartId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable(value = "cartId") String cartId) {
        cartService.delete(cartId);
    }

    @RequestMapping(value = "/add/{productId}", method = RequestMethod.PUT, produces = "application/json")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void addItem(@PathVariable Long productId, HttpServletRequest request) {
        String sessionId = request.getSession(true).getId();
        CartData cart = cartService.read(sessionId);
        if (cart == null) {
            cart = cartService.create(new CartData(sessionId));
        }
        ProductData product = productService.getItemById(productId);
        if (product == null) {
            throw new ApplicationException(DAOError.NO_ITEM_ID, productId);
        }
        CartItemData cartItem = new CartItemData();
        cartItem.setProduct(product);
        cartItem.setQuantity(1L);
        cartItem.setTotalPrice(product.getPrice());
        cart.addCartItem(cartItem);
        cartService.update(sessionId, cart);
    }

    @RequestMapping(value = "/remove/{productId}", method = RequestMethod.PUT, produces = "application/json")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void removeItam(@PathVariable Long productId, HttpServletRequest request) {
        String sessionId = request.getSession(true).getId();
        CartData cart = cartService.read(sessionId);
        if (cart == null || cart.getCartItems() == null || cart.getCartItems().isEmpty()) {
            throw new ApplicationException(DAOError.CART_EMPTY);
        }
        cart.removeCartItem(productId);
        cartService.update(sessionId, cart);
    }

//    @ExceptionHandler(IllegalArgumentException.class)
//    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Wrong request, check transmitted data.")
//    public void handleClientErrors(Exception ex) {
//    }
//
//    @ExceptionHandler(Exception.class)
//    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Internal error server")
//    public void handleServerErrors(Exception ex) {
//    }

}
