package com.packt.cumpelstore.controller;

import com.packt.cumpelstore.data.CartItemData;
import com.packt.cumpelstore.data.OrderData;
import com.packt.cumpelstore.exception.ApplicationException;
import com.packt.cumpelstore.service.OrderService;
import com.packt.cumpelstore.tools.ObjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    private OrderData modifiedOrder;

    @RequestMapping()
    public String list(Model model, Integer offset, Integer maxResults, String orderBy) {
        model.addAttribute("orders", orderService.getList(offset, maxResults, orderBy));
        model.addAttribute("count", orderService.countQuantity());
        model.addAttribute("offset", offset);
        model.addAttribute("orderBy", orderBy);
        return "Order/OrdersList";
    }

    @RequestMapping("/order")
    public String getItemById(Model model, @RequestParam("id") Long id) {
        model.addAttribute("order", orderService.getItemById(id));
        return "Order/OrderDetails";
    }

    @RequestMapping("/removeOrder")
    public String removeOrder(@RequestParam("id") Long id) {
        orderService.removeItem(id);
        return "redirect:/orders";
    }

    @RequestMapping(value = "/modifyOrder", method = RequestMethod.GET)
    public String modifyOrder(Model model, @RequestParam("id") Long id) {
        modifiedOrder = orderService.getItemById(id);
        model.addAttribute("order", modifiedOrder);
        return "Order/OrderModify";
    }

    @RequestMapping(value = "/modifyOrder", method = RequestMethod.POST)
    public String saveModifiedOrder(@ModelAttribute("order") OrderData order) {
        ObjectUtil.getInstance().copyNullProperties(order, modifiedOrder);
        orderService.saveItem(modifiedOrder);
        return "redirect:/orders";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String saveNewOrder(@ModelAttribute("order") OrderData order, @ModelAttribute("products") List<CartItemData> products) {
        orderService.createItem(order);
        return "redirect:/orders";
    }

    @RequestMapping("/{state}")
    public String getOrdersByState(Model model, @PathVariable("state") String orderState) {
        model.addAttribute("orders", orderService.getItemsByState(orderState));
        return "Order/OrdersList";
    }

    @RequestMapping("/{code}/{count}")
    public String process(@PathVariable Long id, @PathVariable Integer count) {
        orderService.processOrder(id, count);
        return "redirect:/products";
    }

    @ExceptionHandler(ApplicationException.class)
    public ModelAndView handleError(HttpServletRequest request, ApplicationException exception) {
        ModelAndView mav = new ModelAndView();
        mav.addObject("invalidOrder", exception.getCodeMessage());
        mav.addObject("exception", exception);
        mav.addObject("url", request.getRequestURI() + "?" + request.getQueryString());
        mav.setViewName("Order/OrderNotFound");
        return mav;
    }
}
