package com.packt.cumpelstore.controller;

import com.packt.cumpelstore.data.CustomerData;
import com.packt.cumpelstore.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
public class HomeController {

	private CustomerService customerService;

	@Autowired
	public HomeController(CustomerService customerService) {
		this.customerService = customerService;
	}

	@RequestMapping("/")
	public String welcome(Model model) {
		return "General/Welcome";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout) {
		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}
		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("General/Login");
		return model;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login";
	}

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public String registration(Model model) {
		CustomerData customer = new CustomerData();
		model.addAttribute("customer", customer);
		return "General/Registration";
	}

	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public String saveNewCustomer(@ModelAttribute("customer") @Valid CustomerData customer, BindingResult result) {
		if (result.hasErrors()) {
			return "General/Registration";
		}
		customer.getUser().setRole("ROLE_USER");
		customerService.createItem(customer);
		return "redirect:/login";
	}
}
