package com.packt.cumpelstore.exception;

import com.packt.cumpelstore.tools.StringUtil;

public class ApplicationException extends RuntimeException {

	private DAOError errorCode;
	
	private String message;

	public ApplicationException(DAOError errorCode, Object... msgParams) {
		super();
		this.errorCode = errorCode;
		this.message = StringUtil.replace(errorCode.getMessage(), msgParams);
	}

	public DAOError getErrorCode() {
		return errorCode;
	}
	
	public String getCodeMessage() {
		return "[" + getErrorCode() + "] " + message;
	}

	public String getMessage() {
		return message;
	}
}
