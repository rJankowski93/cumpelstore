package com.packt.cumpelstore.exception;

public enum DAOError {

    NO_ITEM_ID("No found item by id: \"{0}\""),

    NO_PRODUCT_CATEGORY("No found product by category: \"{0}\""),

    NO_PRODUCT_PROMOTION("No found products in this promotion"),

    NO_PRODUCT_CODE("No found product by code"),

    NO_USER_LOGIN("No found user by login"),

    NO_ORDER_STATE("No found order by state: \"{0}\""),

    CART_EMPTY("Cart is empty"),

    CART_EXIST("Can't create cart. The cart by id: \"{0}\" already exist"),

    CART_NOT_EXIST("Can't update cart. The cart by id:  \"{0}\" don't exist"),

    FEW_PRODUCTS("Too few products");

    String message;

    DAOError(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
