package com.packt.cumpelstore.payment.payu.utils;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.Map;
import java.util.TreeMap;

public class PayuSignature {

    private static final Logger log = Logger.getLogger(PayuSignature.class);

    public static final String MD5_ALGORITHM = "MD5";
    public static final String SHA_ALGORITHM = "SHA-256";

    public static String buildSignature(Map<String, String> payuInputs, String md5Key, String posId, String algorithm) throws UnsupportedEncodingException {
        String hash = calculateSig(payuInputs, md5Key, algorithm);
        return "sender=" + posId + ";algorithm=" + algorithm + ";signature=" + hash;
    }

    public static String calculateSig(Map<String, String> payuInputs, String md5Key, String algorithm) throws UnsupportedEncodingException {

        String message = buildMessage(payuInputs, md5Key);
        try {
            return hash(message, algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException("Could not create signature. Invalid algorithm");
        }
    }

    public static String buildMessage(Map<String, String> payuInputs, String md5Key) throws UnsupportedEncodingException {
        Map<String, String> sortedInputs = new TreeMap<>();
        sortedInputs.putAll(payuInputs);

        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : sortedInputs.entrySet()) {
            sb.append(entry.getKey());
            sb.append("=");
            sb.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            sb.append("&");
        }
        sb.append(md5Key);
        log.info("builded message: " + sb.toString());
        return sb.toString();
    }

    public static String hash(String message, String algorithm) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(algorithm);
        md.update(message.getBytes());
        return toHexString(md.digest());
    }

    private static String toHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        Formatter formatter = new Formatter(sb);
        for (byte b : bytes) {
            formatter.format("%02x", b);
        }
        return sb.toString();
    }
}
