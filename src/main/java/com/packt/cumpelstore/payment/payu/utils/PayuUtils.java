package com.packt.cumpelstore.payment.payu.utils;

import com.packt.cumpelstore.data.*;
import com.packt.cumpelstore.payment.payu.model.PayuData;
import com.packt.cumpelstore.payment.payu.model.PayuProduct;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class PayuUtils {

    private static final Logger logger = Logger.getLogger(PayuUtils.class);

    public static PayuData createPayuData(int posId, String md5Key, String notifyUrl, String continueUrl, String currency, String description, OrderData order, String ipAddress) throws UnsupportedEncodingException {
        PayuData payu = new PayuData(currency);
        payu.setCustomerIp(ipAddress);
        payu.setMerchantPosId(posId);
        payu.setNotifyUrl(notifyUrl);
        payu.setContinueUrl(continueUrl);
        payu.setDescription(description);
        payu.setProducts(order);

        Map<String, String> inputs = PayuUtils.payuDataToMap(payu);
        logger.info("mapa przed przed zakodowaniem: " + inputs.toString());

        String signature = PayuSignature.buildSignature(inputs, md5Key, String.valueOf(posId), PayuSignature.SHA_ALGORITHM);
        payu.setOpenPayuSignature(signature);
        return payu;
    }

    public static Map<String, String> payuDataToMap(PayuData payuData) {
        Map<String, String> inputs = new TreeMap<>();
        inputs.put("customerIp", payuData.getCustomerIp());
        inputs.put("merchantPosId", Integer.toString(payuData.getMerchantPosId()));
        inputs.put("currencyCode", payuData.getCurrencyCode());
        inputs.put("description", payuData.getDescription());
        //TODO create notify controller
        inputs.put("notifyUrl", payuData.getNotifyUrl());
        inputs.put("continueUrl", payuData.getContinueUrl());
        inputs.put("totalAmount", String.valueOf(payuData.getTotalAmount()));

        addProductsToMap(payuData.getProducts(), inputs);

        return inputs;
    }

    private static void addProductsToMap(List<PayuProduct> products, Map<String, String> map) {
        for (int i = 0; i < products.size(); i++) {
            String productIndex = "products[" + i + "].";
            map.put(productIndex + "name", products.get(i).getName());
            map.put(productIndex + "unitPrice", String.valueOf(products.get(i).getUnitPrice()));
            map.put(productIndex + "quantity", String.valueOf(products.get(i).getQuantity()));
        }
    }
}
