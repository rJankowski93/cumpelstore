package com.packt.cumpelstore.payment.payu.model;

import com.packt.cumpelstore.payment.payu.utils.PayuCurrency;

import java.io.Serializable;

public class PayuProduct implements Serializable {
    private String name;
    private long unitPrice;
    private long quantity;

    PayuProduct(String name, long unitPrice, long quantity, String currencyCode) {
        this.name = name;
        this.quantity = quantity;
        this.unitPrice = unitPrice * PayuCurrency.getPenniesAmount(currencyCode);
    }

    public String getName() {
        return name;
    }

    public long getUnitPrice() {
        return unitPrice;
    }

    public long getQuantity() {
        return quantity;
    }
}
