package com.packt.cumpelstore.payment.payu.utils;

public class PayuCurrency {

    public static long getPenniesAmount(String currencyCode) {
        if (currencyCode.equals("PLN"))
            return 100;
        else if (currencyCode.equals("HUF"))
            return 10000;
        else {
            return 100; //default value
        }
    }
}
