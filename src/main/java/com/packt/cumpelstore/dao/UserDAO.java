package com.packt.cumpelstore.dao;

import com.packt.cumpelstore.data.UserData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDAO extends JpaRepository<UserData, Long> {
    List<UserData> getUserByLogin(String login);
}
