package com.packt.cumpelstore.dao;

import com.packt.cumpelstore.data.CustomerData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface CustomerDAO extends JpaRepository<CustomerData, Long> {
    void createItem(CustomerData customerData);

    List<CustomerData> getList(Integer offset, Integer maxResults, String sortBy);

    CustomerData getItemByCurrentUser();

    List<CustomerData> getCustomerByFilter(Map<String, List<Object>> filterParams);

    Long countQuantity();
}
