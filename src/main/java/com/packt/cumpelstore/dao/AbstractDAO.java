package com.packt.cumpelstore.dao;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Transactional
public class AbstractDAO<T> {

    @PersistenceContext
    private EntityManager entityManager;

    private Class<T> type;

    public AbstractDAO(Class<T> type) {
        this.type = type;
    }

    public List<T> getList(Integer offset, Integer maxResults, String sortBy) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(type);
        Root<T> item = cq.from(type);
        cq.select(item);
        if (sortBy != null && !sortBy.equals("")) {
            if (sortBy.endsWith("ASC")) {
                String columnName = sortBy.substring(0, sortBy.length() - 3);
                cq.orderBy(cb.asc(item.get(columnName)));
            } else {
                String columnName = sortBy.substring(0, sortBy.length() - 4);
                cq.orderBy(cb.desc(item.get(columnName)));
            }
        }
        return entityManager.createQuery(cq).setFirstResult(offset != null ? offset : 0)
                .setMaxResults(maxResults != null ? maxResults : 10).getResultList();
    }

    public Long countQuantity() {
        return (Long) entityManager.createQuery("SELECT count(item) FROM " + type.getSimpleName() + " item").getSingleResult();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }
}
