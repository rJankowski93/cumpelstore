package com.packt.cumpelstore.dao.impl;

import com.packt.cumpelstore.dao.AbstractDAO;
import com.packt.cumpelstore.data.CustomerData;
import com.packt.cumpelstore.service.CustomerService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.criteria.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class CustomerDAOImpl extends AbstractDAO<CustomerData> {

    private static final Logger logger = Logger.getLogger(CustomerDAOImpl.class);

    public CustomerDAOImpl() {
        super(CustomerData.class);
    }

    @Autowired
    PasswordEncoder passwordEncoder;


    public void createItem(CustomerData customerData) {
        customerData.setRegistrationDate(new Date());
        customerData.getUser().setPassword(passwordEncoder.encode(customerData.getUser().getPassword()));
        getEntityManager().persist(customerData);
    }

    public CustomerData getItemByCurrentUser() {
        logger.info("------------------------------");
        User user = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<CustomerData> cq = cb.createQuery(CustomerData.class);
        Root<CustomerData> item = cq.from(CustomerData.class);
        cq.select(item).where(cb.equal(item.get("user").get("login"), user.getUsername()));
        return getEntityManager().createQuery(cq).getSingleResult();
    }

    public List<CustomerData> getCustomerByFilter(Map<String, List<Object>> filterParams) throws ParseException {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<CustomerData> cq = cb.createQuery(CustomerData.class);
        Root<CustomerData> r = cq.from(CustomerData.class);
        Predicate predicateAnd = cb.conjunction();
        Predicate predicateOr = null;
        SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
        for (Map.Entry<String, List<Object>> param : filterParams.entrySet()) {
            for (Object o : param.getValue()) {
                if (o == param.getValue().get(0)) {
                    if (param.getKey().equals("city")) {
                        predicateOr = cb.equal(r.get("address").get("city"), o);
                    } else if (param.getKey().equals("country")) {
                        predicateOr = cb.equal(r.get("address").get("country"), o);
                    } else if (param.getKey().equals("login")) {
                        predicateOr = cb.equal(r.get("user").get("login"), o);
                    } else if (param.getKey().equals("registrationDateFrom")) {
                        predicateOr = cb.greaterThanOrEqualTo((Expression) r.get("registrationDate"), formatter.parse(param.getValue().get(0).toString()));
                    } else if (param.getKey().equals("registrationDateTo")) {
                        predicateOr = cb.lessThanOrEqualTo((Expression) r.get("registrationDate"), formatter.parse(param.getValue().get(0).toString()));
                    } else {
                        predicateOr = cb.equal(r.get(param.getKey()), o);
                    }
                } else {
                    predicateOr = cb.or(predicateOr, cb.equal(r.get(param.getKey()), o));
                }
            }
            predicateAnd = cb.and(predicateAnd, predicateOr);
        }
        cq.where(predicateAnd);
        return getEntityManager().createQuery(cq).getResultList();
    }
}
