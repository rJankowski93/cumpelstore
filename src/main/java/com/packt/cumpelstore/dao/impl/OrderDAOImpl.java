package com.packt.cumpelstore.dao.impl;

import com.packt.cumpelstore.dao.AbstractDAO;
import com.packt.cumpelstore.data.OrderData;
import com.packt.cumpelstore.data.PayPalData;
import com.packt.cumpelstore.exception.ApplicationException;
import com.packt.cumpelstore.exception.DAOError;
import com.packt.cumpelstore.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.criteria.*;
import java.util.Date;
import java.util.List;

public class OrderDAOImpl extends AbstractDAO<OrderData> {

    @Autowired
    private CartService cartService;

    @Autowired
    private PayPalData payPalData;

    public OrderDAOImpl() {
        super(OrderData.class);
    }

    //TODO change marge to persist
    public void createItem(OrderData orderData) {
        orderData.setDate(new Date());
        orderData.setState(OrderData.State.PLACED);
        getEntityManager().merge(orderData);
        cartService.delete(orderData.getCart().getId());
    }

    //TODO change to JPQL
    public List<OrderData> getItemsByState(String state) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<OrderData> cq = cb.createQuery(OrderData.class);
        Root<OrderData> productData = cq.from(OrderData.class);
        Expression<String> name = productData.get("state");
        Predicate eq1 = cb.equal(name, state);
        cq.select(productData).where(eq1);
        List<OrderData> ordersByState = getEntityManager().createQuery(cq).getResultList();
        if (ordersByState == null || ordersByState.isEmpty()) {
            throw new ApplicationException(DAOError.NO_ORDER_STATE, state);
        }
        return ordersByState;
    }

    public PayPalData getPayPalConfig() {
        return payPalData;
    }

}
