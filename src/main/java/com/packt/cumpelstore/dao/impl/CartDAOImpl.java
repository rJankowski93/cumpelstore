package com.packt.cumpelstore.dao.impl;

import com.packt.cumpelstore.data.CartData;
import com.packt.cumpelstore.exception.ApplicationException;
import com.packt.cumpelstore.exception.DAOError;

import java.util.HashMap;
import java.util.Map;

public class CartDAOImpl {

    private Map<String, CartData> listOfCarts;

    public CartDAOImpl() {
        listOfCarts = new HashMap<String, CartData>();
    }

    public CartData create(CartData cart) {
        if (listOfCarts.keySet().contains(cart.getId())) {
            throw new ApplicationException(DAOError.CART_EXIST, cart.getId());
        }
        listOfCarts.put(cart.getId(), cart);
        return cart;
    }

    public CartData read(String cartId) {
        return listOfCarts.get(cartId);
    }

    public void update(String cartId, CartData cart) {
        if (!listOfCarts.keySet().contains(cartId)) {
            throw new ApplicationException(DAOError.CART_NOT_EXIST, cart.getId());
        }
        listOfCarts.put(cartId, cart);
    }

    public void delete(String cartId) {
        listOfCarts.remove(cartId);
    }
}
