package com.packt.cumpelstore.validation;

import com.packt.cumpelstore.data.UserData;
import com.packt.cumpelstore.exception.ApplicationException;
import com.packt.cumpelstore.service.CustomerService;
import com.packt.cumpelstore.service.ProductService;
import com.packt.cumpelstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Rafi pedal on 31/07/2016.
 */
public class UniqueCustomerLoginValidator implements ConstraintValidator<UniqueCustomerLogin, UserData> {

    @Autowired
    private ProductService productService;

    @Autowired
    private UserService userService;

    @Autowired
    private CustomerService customerService;

    public void initialize(UniqueCustomerLogin uniqueCustomerLogin) {

    }

    public boolean isValid(UserData value, ConstraintValidatorContext context) {
        if (userService == null) {
            return true;
        }
        try {
            userService.getUserByLogin(value.getLogin());
        } catch (ApplicationException e) {
            return true;
        }
        return false;
    }

}
