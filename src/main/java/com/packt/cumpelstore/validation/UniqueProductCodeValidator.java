package com.packt.cumpelstore.validation;

import com.packt.cumpelstore.exception.ApplicationException;
import com.packt.cumpelstore.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Rafi pedal on 31/07/2016.
 */
public class UniqueProductCodeValidator implements ConstraintValidator<UniqueProductCode, String> {

    @Autowired
    private ProductService productService;

    public void initialize(UniqueProductCode constraintAnnotation) {
        //  celowo pozostawione puste; w tym miejscu należy zainicjować adnotację ograniczającą do sensownych domyślnych w
    }

    public boolean isValid(String value, ConstraintValidatorContext context) {
        try {
            return true;
            //  productService.getProductByCode(value);
        } catch (ApplicationException e) {
            return true;
        }
        // return false;
    }

}
