package com.packt.cumpelstore.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = UniqueProductCodeValidator.class)
@Documented
public @interface UniqueProductCode {
    String message() default "{labels.error.unique.code}";

    Class<?>[] groups() default {};

    public abstract Class<? extends Payload>[] payload() default {};
}
