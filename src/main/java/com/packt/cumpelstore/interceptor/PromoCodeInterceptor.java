package com.packt.cumpelstore.interceptor;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PromoCodeInterceptor extends HandlerInterceptorAdapter {

    private String promoCode;
    private String errorRedirect;
    private String offerRedirect;
    private Boolean promotionCodeIsCorrect;

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        String givenPromoCode = request.getParameterValues("promo") == null ? ""
                : request.getParameterValues("promo")[0];

        if (request.getRequestURI().endsWith("products/specialOffer")) {
            if (givenPromoCode.equals(getPromoCode())) {
                response.sendRedirect(request.getContextPath() + "/" + getOfferRedirect());
                setPromotionCodeIsCorrect(true);
            } else {
                response.sendRedirect(getErrorRedirect());
            }
            return false;
        } else {
            if (request.getRequestURI().endsWith(getOfferRedirect()) && getPromotionCodeIsCorrect() == null) {
                response.sendRedirect(getErrorRedirect());
                return false;
            }
        }
        return true;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getErrorRedirect() {
        return errorRedirect;
    }

    public void setErrorRedirect(String errorRedirect) {
        this.errorRedirect = errorRedirect;
    }

    public String getOfferRedirect() {
        return offerRedirect;
    }

    public void setOfferRedirect(String offerRedirect) {
        this.offerRedirect = offerRedirect;
    }

    public Boolean getPromotionCodeIsCorrect() {
        return promotionCodeIsCorrect;
    }

    public void setPromotionCodeIsCorrect(Boolean promotionCodeIsCorrect) {
        this.promotionCodeIsCorrect = promotionCodeIsCorrect;
    }
}
