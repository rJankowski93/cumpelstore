package com.packt.cumpelstore.data;

import com.packt.cumpelstore.validation.UniqueProductCode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "new_order")
public class OrderData implements Serializable {

    public interface State {
        public static final String PLACED = "Placed";
        public static final String PAID = "Paid";
        public static final String SENT = "Sent";
        public static final String FINISHED = "Finished";
    }

    @Id
    @Column(name = "Id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "Date")
    private Date date;

    @Column(name = "State")
    @UniqueProductCode
    private String state;

    @Column(name = "Customer_Id", insertable = false, updatable = false)
    private Long customerId;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Customer_Id")
    private CustomerData customer;

    @Column(name = "Cart_Id", insertable = false, updatable = false)
    private Long cartId;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "Cart_Id")
    private CartData cart;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public CustomerData getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerData customer) {
        this.customer = customer;
    }

    public Long getCartId() {
        return cartId;
    }

    public void setCartId(Long cartId) {
        this.cartId = cartId;
    }

    public CartData getCart() {
        return cart;
    }

    public void setCart(CartData cart) {
        this.cart = cart;
    }
}
