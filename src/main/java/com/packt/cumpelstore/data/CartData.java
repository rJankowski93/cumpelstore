package com.packt.cumpelstore.data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity(name = "Cart")
public class CartData implements Serializable {

    @Id
    @Column(name = "Id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "Cart_Id", referencedColumnName = "Id")
    private List<CartItemData> cartItems;

    @Column(name = "Grand_Total")
    private Long grandTotal;

    public CartData() {
        cartItems = new ArrayList<CartItemData>();
        grandTotal = 0L;
    }

    public CartData(String cartId) {
        this();
        this.id = cartId;
    }

    public void addCartItem(CartItemData item) {
        String productCode = item.getProduct().getCode();
        for (int i = 0; i < cartItems.size(); i++) {
            if (cartItems.get(i).getProduct().getCode().equals(productCode)) {
                CartItemData existingCartItem = cartItems.get(i);
                existingCartItem.setQuantity(existingCartItem.getQuantity() + item.getQuantity());
                existingCartItem.setTotalPrice(existingCartItem.getQuantity() * existingCartItem.getProduct().getPrice());
                cartItems.set(i, existingCartItem);
                updateGrandTotal();
                return;
            }
        }
        cartItems.add(item);
        updateGrandTotal();
    }

    public void removeCartItem(Long productId) {
        for (CartItemData cartItem : getCartItems()) {
            if (cartItem.getProduct().getId().equals(productId)) {
                cartItems.remove(cartItem);
                break;
            }
        }
        updateGrandTotal();
    }

    public void updateGrandTotal() {
        grandTotal = 0L;
        for (CartItemData item : cartItems) {
            grandTotal = grandTotal + item.getTotalPrice();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Long grandTotal) {
        this.grandTotal = grandTotal;
    }

    public List<CartItemData> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItemData> cartItems) {
        this.cartItems = cartItems;
    }
}
