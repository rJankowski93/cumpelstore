package com.packt.cumpelstore.data;

import com.packt.cumpelstore.validation.UniqueProductCode;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "Product")
public class ProductData implements Serializable {

    public interface Category {
        public static final String PHONE = "Phone";
        public static final String TABLET = "Tablet";
        public static final String LAPTOP = "Laptop";
        public static final String MONITOR = "Monitor";
    }

    public interface State {
        public static final String NEW = "New";
        public static final String SECONDHAND = "Secondhand";
    }

    @Id
    @Column(name = "Id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "Code")
    @NotEmpty(message = "{labels.error.field.empty}")
    @UniqueProductCode
    private String code;

    @Column(name = "Name")
    @NotEmpty(message = "{labels.error.field.empty}")
    private String name;

    @Column(name = "Price")
    @NotNull(message = "{labels.error.field.empty}")
    private Long price;

    @Column(name = "Description")
    private String description;

    @Column(name = "Manufacturer")
    @NotEmpty(message = "{labels.error.field.empty}")
    private String manufacturer;

    @Column(name = "Category")
    private String category;

    @Column(name = "UnitsInStock")
    @NotNull(message = "{labels.error.field.empty}")
    private Long unitsInStock;

    @Column(name = "State")
    private String state;

    @Column(name = "is_Promotion")
    private Boolean isPromotion;

    @Transient
    @Column(name = "Image")
    private MultipartFile productImage;

    public ProductData() {
        super();
    }

    public ProductData(String code, String name, Long unitPrice) {
        this.code = code;
        this.name = name;
        this.price = unitPrice;
    }

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public Long getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getCategory() {
        return category;
    }

    public Long getUnitsInStock() {
        return unitsInStock;
    }

    public String getState() {
        return state;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setUnitsInStock(Long unitsInStock) {
        this.unitsInStock = unitsInStock;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Boolean getIsPromotion() {
        return isPromotion;
    }

    public void setIsPromotion(Boolean isPromotion) {
        this.isPromotion = isPromotion;
    }

    public MultipartFile getProductImage() {
        return productImage;
    }

    public void setProductImage(MultipartFile productImage) {
        this.productImage = productImage;
    }
}
