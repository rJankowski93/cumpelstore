package com.packt.cumpelstore.data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "User_Role")
public class UserRoleData implements Serializable {

    @Id
    @Column(name = "Id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "User_Login")
    // @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "Login")
    private String userLogin;

    @Column(name = "Role")
    private String role;

    public int getId() {
        return id;
    }

    public String getRole() {
        return role;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }
}
