package com.packt.cumpelstore.config;

import com.packt.cumpelstore.data.PayPalData;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class PayPalConfig {

    @Value("${database.user}")
    private String authtoken;
    @Value("${paypal.posturl}")
    private String posturl;
    @Value("${paypal.business}")
    private String business;
    @Value("${paypal.returnurl}")
    private String returnurl;
    @Value("${paypal.cancelurl}")
    private String cancelurl;
    @Value("${paypal.currency_code}")
    private String currency_code;
    @Value("${paypal.cmd}")
    private String cmd;
    @Value("${paypal.charset}")
    private String charset;

    @Bean
    public PayPalData getPayPalSettings() {
        PayPalData payPalData = new PayPalData();
        payPalData.setPosturl(posturl);
        payPalData.setBusiness(business);
        payPalData.setReturnurl(returnurl);
        payPalData.setCmd(cmd);
        payPalData.setCancelurl(cancelurl);
        payPalData.setCurrency_code(currency_code);
        payPalData.setCharset(charset);
        return payPalData;
    }

}
